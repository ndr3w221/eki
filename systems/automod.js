module.exports.autoinvite = (message) => {
  
  if (
    message.content.toLowerCase().match(/(https?:\/\/)?(www\.)?(discord\.(gg|io|me|li)|discordapp\.com\/invite)\/.+[a-z]/)
    && message.guild.settings.get('anti-invite') === true
     )
    try {message.delete()} catch (e) {message.channel.send('Invite Link detected! I can\'t delete it, please give me the Manage Messages permission!')}
  
}