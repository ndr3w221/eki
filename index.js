const express = require('express');
var app = express();

const http = require('http')

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(express.static('public'))

setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 280000);

const { CommandoClient } = require('discord.js-commando');
const path = require('path');
const MongoDBProvider = require('commando-provider-mongo');
const MongoClient = require('mongodb').MongoClient;

const Discord = require('discord.js')

const loggic = require('loggic')
var l = new loggic.logger({
  //tchErrors: true
})

// hope it will update

var { TOKEN, MongoDB, PREFIX, DEV, TOKENDEV } = require('./config.json')

PREFIX = DEV ? 'ee-' : PREFIX

// Initialise commando client
const client = new CommandoClient({
	commandPrefix: PREFIX,
	owner: ["297403616468140032" /* Ankrad#0597 */, 
          "408953935223717898" /* Andrew#6621 */,
          "324920582665928727" /* Deni#0274 */],
	invite: "", 
	disableEveryone: true,
	unknownCommandResponse: false
});

var music = require('telk-music')
// after client is defined
/*music(client, {
  apikey: 'AIzaSyDTv8kEvxGWnO9_B9x27lC31qp28bMe86s', // lol ok
  prefix: "e-",
  emotes: {
    x: "X | ",
    check: "✓",
    mag: "? | "
  }
}); */
// we should abort the music commands for today

client.registry
	.registerDefaultTypes()
	.registerTypesIn(path.join(__dirname, 'types'))
	.registerGroups([
  ["util", "Utility"],
  ["basic", "Basic"],
  ["mod", "Moderation"],
  ['commands', 'Command Manager'],
  ['social', "Social"],
  ['eco', 'Economy'],
  ['fun', 'Fun'],
  ['music', 'Music'],
  ['images', 'Image Commands']
]).registerDefaultCommands({
		help: false,
		ping: false  
	})
	.registerCommandsIn(path.join(__dirname, 'commands'));

app.get('/api/commands', (req, res) => {
  res.send(client.registry.commands)
})

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

app.get('/', (req, res) => {
  let obj = {}
  client.registry.commands.array().forEach(function(cmd) {
    if (obj[cmd.groupID.capitalize()]) obj[cmd.groupID.capitalize()].push(cmd)
    else obj[cmd.groupID.capitalize()] = [cmd]
  })
  res.render('index', {commands: obj})   
}) 

client.on('ready', () => {
  client.user.setActivity(`${PREFIX}help in ${client.guilds.size} guilds`, {type: "WATCHING"})
  console.log(`Logged in as ${client.user.username}`)
})

client.on('guildMemberAdd', (m) => {
  if (m.guild.settings.get('member-logs')) {
    let embed = new Discord.MessageEmbed()
      .setTitle(m.user.tag + ' joined the server')
      .setThumbnail(m.avatarURL)
      .setColor('GREEN')

    client.channels.get(m.guild.settings.get('member-logs')).send({embed})
  }
  if (m.guild.settings.get('welcome-channel')) {
    if (m.guild.settings.get('welcome-message')) {
      client.channels.get(m.guild.settings.get('welcome-channel')).send(m.guild.settings.get('welcome-message').replace('{user.username}', m.user.username).replace('{user}', m.user.username))
    }
  }
  if (m.guild.settings.get('auto-role')) {
    m.roles.add(m.guild.settings.get('auto-role'))
  }
})

client.on('guildMemberRemove', (m) => {
  if (m.guild.settings.get('member-logs')) {
    let embed = new Discord.MessageEmbed()
      .setTitle(m.user.tag + ' left the server')
      .setThumbnail(m.avatarURL)
      .setColor('RED')
    client.channels.get(m.guild.settings.get('member-logs')).send({embed})
  }
  if (m.guild.settings.get('leave-channel')) {
    if (m.guild.settings.get('leave-message')) {
      client.channels.get(m.guild.settings.get('leave-channel')).send(m.guild.settings.get('leave-message').replace('{user.username}', m.user.username).replace('{user}', m.user.username))
    }
  }
})


client.on('message', (...args) => require('./systems/automod.js').autoinvite(...args))

client.on('disconnect', event => {
	console.error(`[DISCONNECT] Disconnected with code ${event.code}.`);
	process.exit(0);
});

client.on('commandRun', command => {
  l.log({
    text: (`Ran command ${command.groupID}:${command.memberName}.`),
    color: 'green'
  }, 'command')
});

client.on('error', err => {
  l.log({
    text: err,
    color: 'red'
  }, 'error')
});

client.on('warn', warn => {
  l.log({
    text: warn,
    color: 'yellow'
  }, 'warning')
});

client.on('commandError', (command, err) => {
  l.log({
    text: `Error running command ${command.name}`,
    color: 'blue'
  }, "error")
});

client.setProvider(
  MongoClient.connect(MongoDB, { useNewUrlParser: true }).then(client => 
    new MongoDBProvider(client, "eskimo")
  )
).catch(console.error);


client.login(DEV ? TOKENDEV : TOKEN);

var listener = app.listen(process.env.PORT, function() {
  console.log('Your app is listening on port ' + listener.address().port);
});