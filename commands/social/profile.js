const Command = require('../../structures/Command');
const Manager = require('../../Manager.js');
const Embed = require('../../utils/embed.js');
const { Canvas } = require('canvas-constructor')
const { MessageAttachment } = require('discord.js')
const moment = require('moment');

const { get } = require("snekfetch"); // This is to fetch the user avatar and convert it to a buffer.
const imageUrlRegex = /\?size=2048$/g;

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'profile',
			aliases: ['prf'], 
			group: 'social', 
			memberName: 'profile',
			description: 'Show and edit your profile',
      args: [
        {
          key: "action",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        },
        {
          key: "arg2",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        },
        {
          key: "arg3",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        }
      ]
		});
	}

	async run(message, {action, arg2, arg3}) {

    var us = message.mentions.users.first() || this.client.users.get(action) /*get user from id*/ || message.author
    if (us.bot) us = message.author;
    var prefix = "e-";
    if (message.guild) prefix = message.guild.commandPrefix;
    
    let user = await Manager.getUser(us.id)

    if (user.length == 0) {
      Manager.add({id : us.id})
      return message.channel.send(`You have created an account. Use this command again to view or set your or other's profile`)
    }
    user = user[0].user;
    console.log(user)
    
    switch (action) {
      case "set":
        if (message.mentions.users.first()) break;
        if (arg2 === 'description') {
          if (!arg3) return message.channel.send(`Check the synatx: \`${prefix}profile set description [something]\`.`)
          let userDescription = await Manager.update(us.id, 'description', arg3.replace('`', ''))
          message.channel.send(`Edited your description to: \`\`\`${arg3}\`\`\``);
        }
        if (arg2 === 'hobby') {
          if (!arg3) return message.channel.send(`Check the synatx: \`${prefix}profile set description [something]\`.`)
          let userDescription = await Manager.update(us.id, 'hobby', arg3.replace('`', ''))
          message.channel.send(`Edited your hobby to: \`\`\`${arg3}\`\`\``);
        }
        break;

      default:
        const { body: avatar } = await get(us.displayAvatarURL({format: "png"}).replace(imageUrlRegex, "?size=128"));
        var canva = new Canvas(350, 400)
         // Setup shwado for reactangles
         .setShadowColor("rgba(22, 22, 22, 1)")
         .setShadowOffsetY(5)
         .setShadowBlur(10)
         // Draw reactangles
         .setColor("#1d1d1d")
         .addRect(0, 0, 350, 400)
         .setColor("#7289DA")
         .addRect(0, 0, 350, 170)
         .setColor('#2C2F33')
         .addRect(20, 230, 310, 125)
         .setShadowColor("rgba(0, 0, 0, 0)") // remove shadow
         // Setup text
         .setColor('#FFFFFF')
         .setTextFont('25px Impact')
         .setTextAlign('center')
         .addText(us.tag, 175, 210) // Write the tag
         .setTextFont('15px Impact') // Redo the text setup for main body
         .setTextAlign('left')
         // Fill in the text
         .addText(`• Balance: ${user.balance ? user.balance : "0"}`, 35,260)
         .addText(`• Description: ${user.description ? user.description : "Not set"}`, 35,285)
         .addText(`• Last Worked: ${user.lastWorked ? moment(user.lastWorked).fromNow() : "Never"}`, 35,310)
         .addText(`• Last Collected: ${user.lastCollected ? moment(user.lastCollected).fromNow() : "Never"}`, 35,335)
         .setTextFont('13px Impact') // Redo text setup for date
         .setTextAlign('right')
         .addText(moment(new Date(Date.now())).format('DD-MM-YYYY'), 330, 380) // Add date mark
         .addCircularImage(avatar, 175, 80, 70) // Plop in the avatar
         .toBuffer(); //make it sendable
        let toSend = new MessageAttachment(canva, "Profile.png"); // format it into discord-able data
        message.channel.send(toSend) // ✈✈✈✈
    }
	}
};
  