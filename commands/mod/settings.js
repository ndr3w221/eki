const Command = require('../../structures/Command');
const Embed = require('../../utils/embed.js');

const dym = require('didyoumean');

module.exports = class SettingsCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'settings',
			aliases: ['config', 'guildconfig', 'guildsettings', 'serverconfig', 'serversettings'], 
			group: 'mod', 
			memberName: 'settings',
			description: 'Show and edit the guild settings',
      args: [
        {
          key: "action",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        },
        {
          key: "arg2",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        },
        {
          key: "arg3",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        }
      ]
		});
	}

	async run(message, {action, arg2, arg3}) {
    if (!message.member.hasPermission('MANAGE_GUILD')) return;
    
    switch (action) {
      case "set":
        switch (arg2) {
          case "member-logs":
          case "members-log": // all of these now do the same thing as there is no `break`
            var c = message.mentions.channels.first();
            if (!c) return message.channel.send(`Check syntax. Eg: \`settings set member-logs #${message.channel.name}\`.`)
            message.guild.settings.set('member-logs', c.id);
            message.channel.send('I set the member-logs for this server, be sure I have perms to send messages!');
            break;
          case "auto-role": 
            var roleNames = message.guild.roles.map(r => r.name)
            var roleName  = dym(arg3, roleNames)
            var roleId    = message.guild.roles.find(r => r.name === roleName).id
            message.guild.settings.set('auto-role', roleId)
            message.channel.send('I set the auto role to : `' + roleName + '`' + ' be sure my roles are above that role just to make sure I have access to give it to every member that is new to the server!')
            
            break;
          case "welcome-channel": 
            var c = message.mentions.channels.first();
            if (!c) return message.channel.send(`Check syntax. Eg: \`settings set welcome-channel #${message.channel.name}\`.`)
            message.guild.settings.set('welcome-channel', c.id);
            message.channel.send('I set the welcome-channel for this server, be sure I have perms to send messages!');
            
            break;
          case "welcome-message": 
            var m = arg3
            message.guild.settings.set('welcome-message', m)
            message.channel.send('I set the welcome-message for this server, Do not forget that {user} will be replaced with user\'s username!');
            break
          case "leave-channel": 
            var c = message.mentions.channels.first();
            if (!c) return message.channel.send(`Check syntax. Eg: \`settings set leave-channel #${message.channel.name}\`.`)
            message.guild.settings.set('leave-channel', c.id);
            message.channel.send('I set the leave-channel for this server, be sure I have perms to send messages!');
            
            break;
          case "leave-message": 
            var m = arg3
            message.guild.settings.set('leave-message', m)
            message.channel.send('I set the leave-message for this server, Do not forget that {user} will be replaced with user\'s username!');
            break
          case "anti-invite": 
            var m = arg3
            if (['enabled', 'on', 'yes'].includes(m)) message.guild.settings.set('anti-invite', true)
            else if (['disabled', 'off', 'no'].includes(m)) message.guild.settings.set('anti-invite', false)
            else return message.channel.send('Avaible options are enabled and disabled!')
            
            message.channel.send('I set the anti-invite for this server, Do not forget give me the manage messages permission!!');
            break
          case "announcements-channel": 
            var c = message.mentions.channels.first();
            if (!c) return message.channel.send(`Check syntax. Eg: \`settings set announcements-channel #${message.channel.name}\`.`)
            message.guild.settings.set('announcements-channel', c.id);
            message.channel.send('I set the announcements-channel for this server, be sure I have perms to send messages!');
            break
          default:
            message.channel.send(`Setting not available. Available keys: \`member-logs\`, \`auto-role\`, \`welcome-channel\`, \`welcome-message\`, \`anti-invite\`.`);
        }
        break;
      default: 
        let e = new Embed(message).embed
          .setTitle('Guild settings')
          .setDescription(`
**Member logs:** ${message.guild.settings.get('member-logs') ? '<#' + message.guild.settings.get('member-logs') + '>' : 'None'}
**Auto role:** ${message.guild.settings.get('auto-role') ? (message.guild.settings.get('auto-role').name ? message.guild.settings.get('auto-role').name : '@InvalidRole') : 'None'}
**Welcome Message:** ${message.guild.settings.get('welcome-message') ? message.guild.settings.get('welcome-message') : 'None'}
**Welcome Channel:** ${message.guild.settings.get('welcome-channel') ? '<#' + message.guild.settings.get('welcome-channel') + '>' : 'None'}
**Leave Message:** ${message.guild.settings.get('leave-message') ? message.guild.settings.get('leave-message') : 'None'}
**Leave Channel:** ${message.guild.settings.get('leave-channel') ? '<#' + message.guild.settings.get('leave-channel') + '>' : 'None'}
**Announcements Channel:** ${message.guild.settings.get('announcements-channel') ? '<#' + message.guild.settings.get('announcements-channel') + '>' : 'None'}
**Anti Invite:** ${message.guild.settings.get('anti-invite') ? 'Enabled' : 'Disabled'}
`)
        message.channel.send({embed: e})
        break;
    }
  }
};
  