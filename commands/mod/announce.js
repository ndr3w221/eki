const Command = require('../../structures/Command');
const Embed = require('../../utils/embed.js');

module.exports = class SettingsCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'announce',
			aliases: ['makeannouncements'], 
			group: 'mod', 
			memberName: 'announce',
			description: 'make an announcement',
      args: [
        {
          key: "action",
          prompt: "What do you want to do", 
          default: "",
          type: "string"
        }
      ]
		});
	}

	async run(message, {action}) {
    if (!message.member.hasPermission('MANAGE_GUILD')) return;
    var c = message.guild.channels.get(message.guild.settings.get('announcements-channel'));
    if (!c) return message.channel.send('You need to set the announcements channel in the e-settings command!')
    
    var t = action
    
    c.send({
      embed: new Embed(message).embed
              .setTitle('New Announcement!')
              .setDescription(t)
              .setFooter('Announcement by ' + message.author.username)
    })
    
    
  }
};
  