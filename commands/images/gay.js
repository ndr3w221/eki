const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed = require('../../utils/embed.js')

const { Canvas } = require('canvas-constructor')
const { MessageAttachment } = require('discord.js')

const { get } = require("snekfetch"); // This is to fetch the user avatar and convert it to a buffer.
const imageUrlRegex = /\?size=2048$/g;

const fsn = require('fs-nextra')

module.exports = class PingCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'gay',
            aliases: [],
            group: 'images',
            memberName: 'gay',
            description: 'Meame',
            ownerOnly: false
        });
    }

    async run(message) {

        var user = message.mentions.users.first() || message.author
        const { body: avatar } = await get(user.displayAvatarURL({format: "png"}).replace(imageUrlRegex, "?size=128"));

        var image = await fsn.readFile('assets/gay.jpg')

        var canv = new Canvas(355 - 50, 267)
            .setColor('#000000')
            .addRect(0, 0, 355 - 50, 267)
            .addImage(avatar, 0, 0, 355 - 50, 267)
            .setGlobalAlpha(0.5)
            .addImage(image, 0, 0, 355 - 50, 267)
            .toBuffer();

        let toSend = new MessageAttachment(canv, "Gay.png"); // format it into discord-able data
        message.channel.send(toSend) // ✈✈✈✈
    }
};
