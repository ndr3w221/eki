const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed = require('../../utils/embed.js')

const { Canvas } = require('canvas-constructor')
const { MessageAttachment } = require('discord.js')

const { get } = require("snekfetch"); // This is to fetch the user avatar and convert it to a buffer.
const imageUrlRegex = /\?size=2048$/g;

const fsn = require('fs-nextra')

module.exports = class PingCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'beautiful',
            aliases: [],
            group: 'images',
            memberName: 'beautiful',
            description: 'Meame',
            ownerOnly: false
        });
    }

    async run(message) {

        var user = message.mentions.users.first() || message.author
        const { body: avatar } = await get(user.displayAvatarURL({format: "png"}).replace(imageUrlRegex, "?size=128"));

        var image = await fsn.readFile('assets/beautiful.png')

        var canv = new Canvas(634, 675)
            .setColor('#000000')
            .addRect(0, 0, 634, 675)
            .addImage(image, 0, 0, 634, 675)
            .setColor('#1d1d1d')
            .addImage(avatar, 435, 43, 150, 170)
            .addImage(avatar, 435, 380, 150, 170)
            .toBuffer();

        let toSend = new MessageAttachment(canv, "Beautiful.png"); // format it into discord-able data
        message.channel.send(toSend) // ✈✈✈✈
    }
};
