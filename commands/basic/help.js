const Command = require('../../structures/Command');
const fs = require('fs');
const { MessageEmbed } = require('discord.js');

module.exports = class HelpCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'help',
			aliases: ['cmds', 'commands'],
			group: 'basic',
			memberName: 'help',
			description: 'Sends you help',
			guarded: true
		});
	};

	async run(message, args) {
    const data = this.client.registry.commands.array();
    let mod = '', basic = '', utility = '', economy = '', commands = '', fun = '', images = '';
    let prefix = "ee-";
    if (message.guild) prefix = message.guild.commandPrefix
    for (var i = 0; i < data.length; i++) {
      let command = data[i];
      let sample
      if (!command.ownerOnly) {
        
        sample  = `\n\`${command.name}\`: \`${command.description}\`${(command.ownerOnly ? ' __**Owner Only**__ ' : '')} ${(command.nsfw ? ' __**NSFW**__' : '')}`;
      
      

        switch(command.groupID) {
          case "mod":
            mod += sample;
            break;
          case "basic":
            basic += sample;
            break
          case "util":
            utility += sample;
            break;
          case "eco":
            economy += sample;
            break;
          case "fun":
            fun += sample;
            break;
          case "commands":
            commands += sample
            break;
          case "images":
            images += sample
          }
      }
    }
      console.log()
    let embed = new MessageEmbed()
      .setTitle('All my commands!')
      .setColor('#36393E')
      .setDescription(`
My prefix is ${prefix}

**Basic commands** ${basic}
` /*+ `**Fun commands** ${fun}`*/ +  ` 
**Moderator commands** ${mod}
**Utility commands** ${utility}
**Economy commands** ${economy}
**Command management commands** ${commands}
**Image commands** ${images}

__**[Join the support server](https://discord.gg/bEjgA2m)**__
     `, true)
      .setFooter('For reports contact ndr3w221#6621 or Ankrad#0597');
    message.channel.send(embed)
  }
};
