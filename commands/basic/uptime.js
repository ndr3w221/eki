const Command = require('../../structures/Command');

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'uptime',
			aliases: ["up"],
			group: 'basic',
			memberName: 'uptime',
			description: 'Checks the bot\'s uptime',
			guarded: true
		});
	}

	async run(message) {
    let u  = Math.round(message.client.uptime/1000/60)
		return message.channel.send(`${u} minute${u === 1 ? "" : "s"}`);
	}
};
