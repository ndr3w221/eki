const Command = require('../../structures/Command');
const Embed = require('../../utils/embed.js')

var moment = require("moment");

module.exports = class ServerInfoCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'serverinfo',
			aliases: ['server', 'guild', 'guildinfo'],
			group: 'util',
			memberName: 'serverinfo',
			description: 'Check info about the guild.',
      guildOnly: true
		});
	}

	async run(message) {
   var guild = message.guild

    var verificationLevel = [
        '**None**\n(Unrestricted)',
        '**Low**\n(Must have verified email on account)',
        '**Medium**\n(Must be registered on Discord for longer than 5 minutes)',
        '**High**\n(Must be a member of the server for longer than 10 minutes)',
        '**Very High**\n(Must have a verified phone number)'
    ]
    var explicitContentFilter = [
      '**Level 1**\n(Don\'t scan any messages)',
      '**Level 2**\n(Scan messages from members without a role)',
      '**Level 3**\n(Scan all messages.)'
    ]

    let m = `
All memebers : ${guild.memberCount}

Bots : ${guild.members.filter(m=>m.user.bot).size}
Humans : ${guild.memberCount - message.guild.members.filter(m=>m.user.bot).size}
`
    
    let embed = new Embed(message).embed // just....
    .setTitle(`Server info ${guild.name}`)
    .addField(`ID`, guild.id)
    .addField(`Server Verification Level`, verificationLevel[guild.verificationLevel])
    .addField(`Server Explicit Content filter`, explicitContentFilter[guild.explicitContentFilter])
    .addField(`Region`, guild.region)
    .addField(`Created at`, moment.utc(guild.createdAt).format("dddd, MMMM Do YYYY, HH:mm:ss"))
    .addField(`Large`, guild.large ? "Yes" : "No")
    .addField(`Owner (Ownership)`, guild.owner)
    .addField(`Members`, m)

    message.channel.send(embed)

	}
};
