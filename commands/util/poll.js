const Command = require('../../structures/Command');
const Embed = require('../../utils/embed.js')

const emojiList = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣', '🔟']; 

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'poll',
			aliases: ['polls'],
			group: 'util',
			memberName: 'poll',
			description: 'Make polls easily.'
		});
	}

	async run(message, args) {
    var splited = args.split(' | ');
    var title = splited[0]; 
    if (!title) return message.channel.send(`Choose a title`)
    var options = splited.splice(1)
    if (options.length >= 10) return message.channel.send('The limit of options is **10**.')
    if (options.length <= 1) return message.channel.send('You need to add options using `|` to separate them.')
    var embed = new Embed(message).embed
        .setTitle('Poll! - ' + title)
    var res = ''
    for (var i = 0; i < options.length; i++) {
        res += emojiList[i] + ' ' + options[i] + '\n' 
    }
    embed.setDescription(res)
    message.channel.send({
        embed
    }).then(async function (m) {  
        var reactionArray = [];
        for (var i = 0; i < options.length; i++) {
            reactionArray[i] = await m.react(emojiList[i]);
        }
    })
	}
};
