const Command = require('../../structures/Command');

const snek = require('snekfetch');
const moment = require('moment');
require('moment-duration-format');


const Embed = require('../../utils/embed.js');

module.exports = class NpmCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'githubuser',
			group: 'util',
      aliases: ['ghuser', 'user-github', 'github-user'],
			memberName: 'githubuser',
			description: 'Search a user from github',
      args: [
        {
          key: "term",
          prompt: "You must specify a term", 
          default: undefined,
          type: "string"
        }
        
        ]
		});
	}

	async run(message, {term}) {
      
          const {
              body
          } = await snek.get(`https://api.github.com/users/${term}`)
    
          let embed = new Embed(message).embed
          .setTitle('Github user: ' + term)
          .setDescription(`
Following: ${body.following}
Followers: ${body.followers}
Repositories: ${body.public_repos}
Gists: ${body.public_gists}
`)
          .setThumbnail(body.avatar_url)
  
          message.channel.send({embed})
	}
};