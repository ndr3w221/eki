const Command = require('../../structures/Command');

const snek = require('snekfetch');
const moment = require('moment');
require('moment-duration-format');


const Embed = require('../../utils/embed.js');

module.exports = class NpmCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'npm',
			group: 'util',
      aliases: [],
			memberName: 'npm',
			description: 'Search a Node Package',
      args: [
        {
          key: "search",
          prompt: "You must specify a search term", 
          default: "",
          type: "string"
        }
        
        ]
		});
	}

	async run(message, {search}) {
      try {
          const {
              body
          } = await snek.get(`https://registry.npmjs.com/${search.toLowerCase()}`);
          const version = body.versions[body['dist-tags'].latest];
          let deps = version.dependencies ? Object.keys(version.dependencies) : null;
          let maintainers = body.maintainers.map(user => user.name);
          let github = version.repository.url
          let gitshort = github.slice(23, -4)

          if (maintainers.length > 10) {
              const len = maintainers.length - 10;
              maintainers = maintainers.slice(0, 10);
              maintainers.push(`...${len} more.`);
          }

          if (deps && deps.length > 10) {
              const len = deps.length - 10;
              deps = deps.slice(0, 10);
              deps.push(`...${len} more.`);
          }

          function customTemplate() {
              return this.duration.asSeconds() >= 86400 ? "w [weeks], d [days]" : "h [hrs], m [mins], s [secs]";
          }

          let updated = moment.duration(Date.now() - new Date(body.time[body['dist-tags'].latest]).getTime()).format(customTemplate, {
              trim: false
          });

        let e = new Embed(message).embed
        .setTitle('Info npmjs package')
        .addField("Description", `${version.description || 'No description.'}\n\u200B`)
        .addField("Last Modified", `${updated} ago`)
        .addField("Version", `${body['dist-tags'].latest}`)
        .addField("Maintainers", maintainers.join(', '))
        .addField("License", `${body.license}\n\u200B`)
        .addField("Dependencies", `${deps && deps.length ? deps.join(', ') : '*None*'}\n\u200B`)
        .addField(`NPM.js package link`, `[\`https://www.npmjs.com/package/${search.toLowerCase()}\`](https://www.npmjs.com/package/${search.toLowerCase()})`)
        .addField(`Github repo link`, `[\`https://www.github.com/${gitshort}\`](https://www.github.com/${gitshort})`)
        message.channel.send(e)
      } catch (error) {
          if (error.status == 404) return message.channel.send('Error 404: Module not found.');
          console.log(error);
      }
	}
};