const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed   = require('../../utils/embed.js')
const os      = require("os")
const Discord = require('discord.js')

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'stats',
			aliases: ['statistics'],
			group: 'util',
			memberName: 'stats',
			description: 'See bot\'s stats.'
		});
  }

	async run(message) { 
    var inline = true

    const embed = new Embed(message).embed
      .setTitle("Stats")
      .setTimestamp()
      .addField("Discord.js version ", "v" + Discord.version, inline)
      .addField("NodeJS version ", process.version, inline)
      .addField("Server count ", this.client.guilds.size, inline)
      .addField("Total Members ", this.client.users.size, inline)
      .addField("Total Channels ", this.client.channels.size, inline)
      .addField("Platform", process.platform, inline)
      .addField("Home dir", os.homedir(), inline)
      .addField("Host name", os.hostname(), inline)
      .addField("Temp dir", os.tmpdir(), inline)
    message.channel.send({
      embed
    });
  }
};
        