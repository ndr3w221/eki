const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed   = require('../../utils/embed.js')

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'add',
			aliases: ['plus'],
			group: 'eco',
			memberName: 'add',
			description: 'Add balance to a user',
      ownerOnly: true,
      args: [
        {
          key: "amount",
          prompt: "Amount to add", 
          default: "",
          type: "string"
        },
        {
          key: "mention",
          prompt: "Mention someone to add for", 
          default: "",
          type: "user"
        }
        ]
		});
  }

	async run(message, {amount, mention}) {
    if (amount == "" || mention == "") return message.channel.send(`Check syntax: \`add [amount] [user (ping someone)]\`.`);
    if (parseInt(amount) === undefined) return message.channel.send(`Check syntax: \`add [amount] [user (ping someone)]\`.`);
    
    amount = parseInt(amount)
    
    let res = await Manager.increment(mention.id, "balance", amount)
    
    if (res === false) return message.channel.send(`Unknown error.`)
    else return message.channel.send(`Added \`${amount}\` coins to \`${mention.username}\`'s account  `)
  }
};
