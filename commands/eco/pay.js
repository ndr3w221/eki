const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed   = require('../../utils/embed.js')

module.exports = class PayCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'pay',
			aliases: ['give'],
			group: 'eco',
			memberName: 'pay',
			description: 'Pay someone your coins.',
      guildOnly: true,
      args: [
        {
          key: "amount",
          prompt: "Amount to pay", 
          default: "",
          type: "string"
        },
        {
          key: "mention",
          prompt: "Mention someone to pay", 
          default: "",
          type: "member"
        }
        ]
		});
	}

	async run(message, {amount, mention}) {
    amount =  parseInt(amount);
    if (!amount || !mention) return message.channel.send(`Check syntax`);
    
    if (amount < 0) return message.channel.send(`:wink: n0pe!`);
    
    var user = await Manager.getUser(message.author.id);
    if (!user) {
      await Manager.add({id: message.author.id});
      return message.channel.send('You don\'t have an account, I created one for you! Just type this command again and you will be able to run it.');
    }
    if (user[0].user.balance < amount) return message.channel.send(`You don't have enough...`)
    var user2 = await Manager.getUser(mention.user.id);
    if (!user2) return message.channel.send(`The person you mentioned doesn't have an account. They need to use the \`profile\` command to create one.`)
    
    // Do confirmation here : are you sure you want to give [x],  [y] coins
    
    await Manager.increment(message.author.id, "balance", -amount);
    await Manager.increment(mention.user.id, "balance", amount);
    
    message.channel.send(`\`${message.member.displayName}\` has given \`${mention.displayName}\` \`${amount} coins\`.`)
	}
};
