const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed   = require('../../utils/embed.js')

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'balance',
			aliases: ['bal'],
			group: 'eco',
			memberName: 'balance',
			description: 'Check your balance!'
		});
  }

	async run(message) { 
		var user = await Manager.getUser(message.author.id);
    
    if (user.length == 0) {
      await Manager.add({id: message.author.id});
      return message.channel.send('You don\'t have an account, I created one for you! Just type this command again and you will see your balance');
    }
    user = user[0].user;
    if (user['balance'] == undefined) user.balance = 0;
    
    let embed = new Embed(message).embed
      .setTitle(`${message.author.username}'s balance`)
      .setDescription(`${user.balance} coins`)
    message.channel.send(embed);
  }
};
