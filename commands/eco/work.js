const Command = require('../../structures/Command');
const Manager = require('../../Manager.js');
const Embed   = require('../../utils/embed.js');
const INCREMENT_AMOUNT =  50;

module.exports = class WorkCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'work',
			aliases: [],
			group: 'eco',
			memberName: 'work',
			description: 'Work!'
		});
  }

	async run(message) { 
		var user = await Manager.getUser(message.author.id);
    
    if (user.length == 0) {
      await Manager.add({id: message.author.id});
      return message.channel.send('You don\'t have an account, I created one for you! Just type this command again and you will be able to use this command.');
    }
    user = user[0].user;
    if (user.balance === undefined) user.balance = 0;
    if (user.lastWorked === undefined) user.lastWorked = 0;
    if (Date.now() - parseInt(user.lastWorked) < 3600000) return message.channel.send(`You have already collected in the last day.`)
    else {
      await Manager.update(message.author.id, "lastWorked", Date.now())
      await Manager.increment(message.author.id, "balance", INCREMENT_AMOUNT); 
		  var newUser = await Manager.getUser(message.author.id);
      message.channel.send(`You worked, you can work again in 2 hours! Now you have ${newUser[0].user.balance} coins!`)
    }
  }
};
