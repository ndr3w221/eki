const Command = require('../../structures/Command');
const Manager = require('../../Manager.js')
const Embed   = require('../../utils/embed.js')

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'lotto',
			aliases: ['loto', 'lt'],
			group: 'eco',
			memberName: 'lotto',
			description: 'Win coins!'
		});
	}

	async run(message) {
    var user = await Manager.getUser(message.author.id);
    
    
    if (!user) {
      await Manager.add({id: message.author.id});
      return message.channel.send('You don\'t have an account, I created one for you! Just type this command again and you will be able to run it.');
    }
    
    if (user.balance < 5) return message.channel.send(`You can't do \`lotto\` with less than 5 coins.`)
    
    let res = await Manager.lotto(message.author.id);
    
    if (res) return message.channel.send('You won 5 coins!');
    else return message.channel.send('You lost 5 coins!');
	}
};
