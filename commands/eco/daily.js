const Command = require('../../structures/Command');
const Manager = require('../../Manager.js');
const Embed   = require('../../utils/embed.js');
const INCREMENT_AMOUNT =  200;

module.exports = class DailyCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'daily',
			aliases: ['collect'],
			group: 'eco',
			memberName: 'daily',
			description: 'Collect your daily coins!'
		});
  }

	async run(message) { 
		var user = await Manager.getUser(message.author.id);
    
    if (user.length == 0) {
      await Manager.add({id: message.author.id});
      return message.channel.send('You don\'t have an account, I created one for you! Just type this command again and you will be able to run this command.');
    }
    user = user[0].user;
    if (user.balance === undefined) user.balance = 0;
    if (user.lastCollected === undefined) user.lastCollected = 0;
    if (Date.now() - parseInt(user.lastCollected) < 86400000) return message.channel.send(`You have already collected in the last day.`)
    else {
      await Manager.update(message.author.id, "lastCollected", Date.now())
      await Manager.increment(message.author.id, "balance", INCREMENT_AMOUNT); 
		  var newUser = await Manager.getUser(message.author.id);
      message.channel.send(`You now have ${newUser[0].user.balance} coins.`)
    }
  }
};
