const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
var {MongoDB} = require('./config.json');

function getUser(id) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(MongoDB, { useNewUrlParser: true }, function(err, client) {
      assert.equal(null, err);
      const db = client.db('eskimo');
      const collection = db.collection('users');
      collection.find({"user.id": id}).toArray(function(err, docs) {
        if (err !== null) return resolve(false);
        resolve(docs)
      });
    })
  })
}

function add(user) { 
  return new Promise((resolve, reject) => { 
    MongoClient.connect(MongoDB, { useNewUrlParser: true }, function(err, client) {
      assert.equal(null, err);
      const db = client.db('eskimo');
      const collection = db.collection('users');
      collection.insertMany([{user: user}], function(err, docs) {
        assert.equal(err, null);
        resolve(docs);
      });
    })
  })
}

function update(userId, key, value) {
  return new Promise((resolve, reject) => { 
    MongoClient.connect(MongoDB, { useNewUrlParser: true }, function(err, client) {
      assert.equal(null, err);
      const db = client.db('eskimo');
      const collection = db.collection('users');
      var change = {};
      change[`user.${key}`] = value;
      collection.updateOne({"user.id": userId} , { $set: change}, function(err, result) {
        if (err !== null) return resolve(false);
        else if (result.result.n !== 1) return resolve(false);
        else return resolve(true);
      });  
        
    })
  })
}



function increment(userId, key, value) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(MongoDB, { useNewUrlParser: true }, function(err, client) {
      assert.equal(null, err);
      const db = client.db('eskimo');
      const collection = db.collection('users');
      var obj = {};
      obj[`user.${key}`] = value;
      collection.updateOne({"user.id": userId} , { $inc: obj}, function(err, result) {
        if (err !== null) return resolve(false);
        else if (result.result.n !== 1) return resolve(false);
        else return resolve(true);
      });
    })
  })
}




function drop() {
  return new Promise((resolve, reject) => { 
    MongoClient.connect(MongoDB, { useNewUrlParser: true }, function(err, client) {
      assert.equal(null, err);
      const db = client.db('eskimo');
      const collection = db.collection('users');
      collection.drop()
      resolve(true)
    })
  })
}

function lotto(userId) {
  return new Promise((resolve, reject) => {
    const rand = Boolean(Math.round(Math.random()));
    if (rand) {
      increment(userId, 'balance', 5).then(() => {resolve(true)})
    } else {
      increment(userId, 'balance', -5).then(() => {resolve(false)})
    }
  })
}

module.exports.getUser = function(u) {return getUser(u)};
module.exports.add = function(u) {return add(u)};
module.exports.update = function(u, k, v) {return update(u, k, v)};
module.exports.drop = function() {return drop()};
module.exports.lotto = function(u, c) {return lotto(u, c)};
module.exports.increment = function(u, k, v) {return increment(u, k, v)};