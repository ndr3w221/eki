var { MessageEmbed } = require('discord.js')

module.exports = class Embed {
  constructor(message) {
    let e = new MessageEmbed()
        .setAuthor(message.client.user.username, message.client.user.displayAvatarURL())
        .setFooter('Command Triggered by ' + message.author.tag, message.author.displayAvatarURL())
        .setTimestamp()
        .setColor(0x36393E) 
    this.embed = e;
  }
}